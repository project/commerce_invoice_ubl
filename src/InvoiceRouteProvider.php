<?php

namespace Drupal\commerce_invoice_ubl;

use Drupal\commerce_invoice\InvoiceRouteProvider as InvoiceRouteProviderBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\Routing\Route;

class InvoiceRouteProvider extends InvoiceRouteProviderBase {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($download_ubl_route = $this->getDownloadUblRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.download_ubl", $download_ubl_route);
    }

    return $collection;
  }

  /**
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *
   * @return \Symfony\Component\Routing\Route|void
   */
  protected function getDownloadUblRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('download_ubl')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('download_ubl'));
      $route
        ->addDefaults([
          '_controller' => '\Drupal\commerce_invoice_ubl\Controller\InvoiceController::downloadUbl',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.view")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

}
