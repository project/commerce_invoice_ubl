<?php

namespace Drupal\commerce_invoice_ubl;

use Drupal\commerce_invoice\Entity\InvoiceInterface;
use Drupal\commerce_invoice\Entity\InvoiceItem;
use Drupal\commerce_invoice\InvoiceFileManagerInterface;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_store\Entity\Store;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\profile\Entity\Profile;
use NumNum\UBL\AdditionalDocumentReference;
use NumNum\UBL\Address;
use NumNum\UBL\Attachment;
use NumNum\UBL\Contact;
use NumNum\UBL\Country;
use NumNum\UBL\Generator;
use NumNum\UBL\Invoice;
use NumNum\UBL\InvoiceLine;
use NumNum\UBL\InvoiceTypeCode;
use NumNum\UBL\Item;
use NumNum\UBL\LegalEntity;
use NumNum\UBL\LegalMonetaryTotal;
use NumNum\UBL\Party;
use NumNum\UBL\PartyTaxScheme;
use NumNum\UBL\PaymentMeans;
use NumNum\UBL\Price;
use NumNum\UBL\TaxCategory;
use NumNum\UBL\TaxScheme;
use NumNum\UBL\TaxSubTotal;
use NumNum\UBL\TaxTotal;
use NumNum\UBL\UnitCode;

class UblBuilder {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\commerce_invoice\InvoiceFileManagerInterface
   */
  private InvoiceFileManagerInterface $invoiceFileManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\commerce_invoice\InvoiceFileManagerInterface $invoiceFileManager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   */
  public function __construct(FileSystemInterface $fileSystem, InvoiceFileManagerInterface $invoiceFileManager, ModuleHandlerInterface $moduleHandler) {
    $this->fileSystem = $fileSystem;
    $this->invoiceFileManager = $invoiceFileManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * @param array $invoices
   * @param string $scheme
   * @param string $filename
   *
   * @return string
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function savePrintable(array $invoices, string $scheme, string $filename): string {
    return $this->fileSystem->saveData($this->getBlob($invoices), "$scheme://$filename", FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * @param array $invoices
   *
   * @return string
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getBlob(array $invoices): string {
    /** @var \Drupal\commerce_invoice\Entity\Invoice $invoice */
    $invoice = reset($invoices);
    $orders = $invoice->getOrders();
    $order = reset($orders);

    $store = $invoice->getStore();
    $taxPercentage = 21;
    $totalIncl = ($invoice->getTotalPrice())->getNumber();
    $totalExcl = $totalIncl / (1 + ($taxPercentage / 100));
    $tax = $totalIncl - $totalExcl;

    $legalMonetaryTotal = (new LegalMonetaryTotal())
      ->setLineExtensionAmount($totalExcl)
      ->setTaxExclusiveAmount($totalExcl)
      ->setTaxInclusiveAmount($totalIncl)
      ->setPayableAmount($totalIncl);

    $taxCategory = (new TaxCategory())
      ->setId(0)
      ->setName("VAT$taxPercentage%")
      ->setPercent($taxPercentage)
      ->setTaxScheme((new TaxScheme()));

    $taxSubTotal = (new TaxSubTotal())
      ->setTaxableAmount($totalExcl)
      ->setTaxAmount($tax)
      ->setTaxCategory($taxCategory);

    $instructionId = t('@store order @order_id', [
      '@store' => $store->label(),
      '@order_id' => $order->id(),
    ]);
    $this->moduleHandler->alter('ubl_instruction_id', $instructionId, $store, $order);

    $paymentMeans = (new PaymentMeans())
      ->setInstructionId($instructionId);

    $taxTotal = (new TaxTotal())
      ->addTaxSubTotal($taxSubTotal)
      ->setTaxAmount($tax);

    $invoiceLines = array_merge(
      $this->getItemsInvoiceLines($invoice->getItems(), $taxPercentage),
      $this->getAdjustmentsInvoiceLines($invoice->getAdjustments(), $taxPercentage)
    );

    $invoice = (new Invoice())
      ->setId($invoice->getInvoiceNumber())
      ->setInvoiceTypeCode($invoice->bundle() === 'credit_memo' ? InvoiceTypeCode::CREDIT_NOTE : InvoiceTypeCode::INVOICE)
      ->setCopyIndicator(false)
      ->setIssueDate(new \DateTime('@' . $invoice->getInvoiceDateTime()))
      ->setDocumentCurrencyCode($store->getDefaultCurrencyCode())
      ->setAdditionalDocumentReference($this->getPdfAttachment($invoice))
      ->setAccountingSupplierParty($this->getAccountingSupplierParty($store))
      ->setAccountingCustomerParty($this->getAccountingCustomerParty($invoice->getBillingProfile(), $invoice->getEmail()))
      ->setInvoiceLines($invoiceLines)
      ->setLegalMonetaryTotal($legalMonetaryTotal)
      ->setPaymentMeans($paymentMeans)
      ->setTaxTotal($taxTotal);

    return (new Generator())->invoice($invoice);
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   *
   * @return \NumNum\UBL\AdditionalDocumentReference
   */
  private function getPdfAttachment(InvoiceInterface $invoice): AdditionalDocumentReference {
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->invoiceFileManager->getInvoiceFile($invoice);

    $attachment = (new Attachment())
      ->setFilePath($file->getFileUri());

    return (new AdditionalDocumentReference())
      ->setId('Document')
      ->setAttachment($attachment);
  }

  /**
   * @param \Drupal\commerce_store\Entity\Store $store
   *
   * @return \NumNum\UBL\Party
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getAccountingSupplierParty(Store $store): Party {
    $storeAddress = $store->getAddress();

    $country = (new Country())
      ->setIdentificationCode($storeAddress->get('country_code')->getValue());

    $address = (new Address())
      ->setStreetName($storeAddress->get('address_line1')->getValue())
      ->setBuildingNumber($storeAddress->get('address_line2')->getValue())
      ->setCityName($storeAddress->get('locality')->getValue())
      ->setPostalZone($storeAddress->get('postal_code')->getValue())
      ->setCountry($country);

    $contact = (new Contact())
      ->setElectronicMail($store->getEmail());

    $taxSchema = (new PartyTaxScheme())
      ->setCompanyId($store->get('field_tax_number')->value);

    $legalEntity = (new LegalEntity())
      ->setCompanyId($store->get('field_tax_number')->value)
      ->setRegistrationName($store->get('field_registration_name')->value);

    return (new Party())
      ->setName($store->getName())
      ->setPhysicalLocation($address)
      ->setPostalAddress($address)
      ->setContact($contact)
      ->setPartyTaxScheme($taxSchema)
      ->setLegalEntity($legalEntity);
  }

  /**
   * @param \Drupal\profile\Entity\Profile $billingProfile
   * @param string $email
   *
   * @return \NumNum\UBL\Party
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  private function getAccountingCustomerParty(Profile $billingProfile, string $email): Party {
    $profileAddress = $billingProfile->get('address')->first();

    $name = implode(' ', array_filter([
      $profileAddress->get('given_name')->getValue(),
      $profileAddress->get('additional_name')->getValue(),
      $profileAddress->get('family_name')->getValue(),
    ]));

    $country = (new Country())
      ->setIdentificationCode($profileAddress->get('country_code')->getValue());

    $address = (new Address())
      ->setStreetName($profileAddress->get('address_line1')->getValue())
      ->setBuildingNumber($profileAddress->get('address_line2')->getValue())
      ->setCityName($profileAddress->get('locality')->getValue())
      ->setPostalZone($profileAddress->get('postal_code')->getValue())
      ->setCountry($country);

    $contact = (new Contact())
      ->setName($name)
      ->setElectronicMail($email);

    $taxSchema = (new PartyTaxScheme())
      ->setCompanyId($billingProfile->get('tax_number')->value);

    $legalEntity = (new LegalEntity())
      ->setCompanyId($billingProfile->get('tax_number')->value)
      ->setRegistrationName($billingProfile->get('field_company')->value);

    return (new Party())
      ->setName($name)
      ->setPhysicalLocation($address)
      ->setPostalAddress($address)
      ->setContact($contact)
      ->setPartyTaxScheme($taxSchema)
      ->setLegalEntity($legalEntity);
  }

  /**
   * @param array $items
   * @param int $taxPercentage
   *
   * @return array
   */
  private function getItemsInvoiceLines(array $items, int $taxPercentage): array {
    return array_map(static function (InvoiceItem $item) use ($taxPercentage) {
      $totalIncl = ($item->getAdjustedTotalPrice())->getNumber();
      $totalExcl = $totalIncl / (1 + ($taxPercentage / 100));
      $tax = $totalIncl - $totalExcl;

      $productItem = (new Item())
        ->setName($item->label());

      $price = (new Price())
        ->setBaseQuantity(1)
        ->setUnitCode(UnitCode::UNIT)
        ->setPriceAmount(($item->getAdjustedUnitPrice())->getNumber());

      $taxCategory = (new TaxCategory())
        ->setName("VAT$taxPercentage%")
        ->setPercent($taxPercentage)
        ->setTaxScheme((new TaxScheme()));

      $taxSubTotal = (new TaxSubTotal())
        ->setTaxableAmount($totalExcl)
        ->setTaxAmount($tax)
        ->setTaxCategory($taxCategory);

      $taxTotal = (new TaxTotal())
        ->addTaxSubTotal($taxSubTotal)
        ->setTaxAmount($tax);

      return (new InvoiceLine())
        ->setId($item->id())
        ->setItem($productItem)
        ->setPrice($price)
        ->setTaxTotal($taxTotal)
        ->setInvoicedQuantity($item->getQuantity())
        ->setLineExtensionAmount($totalExcl);
    }, $items);
  }

  private function getAdjustmentsInvoiceLines(array $adjustments, int $taxPercentage): array {
    // Tax is calculated on each invoice line.
    $adjustments = array_filter($adjustments, static function (Adjustment $adjustment) {
      return $adjustment->getType() !== 'tax';
    });

    return array_map(static function (Adjustment $adjustment) use ($taxPercentage) {
      $totalIncl = ($adjustment->getAmount())->getNumber();
      $totalExcl = $totalIncl / (1 + ($taxPercentage / 100));
      $tax = $totalIncl - $totalExcl;

      $productItem = (new Item())
        ->setName($adjustment->getLabel());

      $price = (new Price())
        ->setBaseQuantity(1)
        ->setUnitCode(UnitCode::UNIT)
        ->setPriceAmount(($adjustment->getAmount())->getNumber());

      $taxCategory = (new TaxCategory())
        ->setName("VAT$taxPercentage%")
        ->setPercent($taxPercentage)
        ->setTaxScheme((new TaxScheme()));

      $taxSubTotal = (new TaxSubTotal())
        ->setTaxableAmount($totalExcl)
        ->setTaxAmount($tax)
        ->setTaxCategory($taxCategory);

      $taxTotal = (new TaxTotal())
        ->addTaxSubTotal($taxSubTotal)
        ->setTaxAmount($tax);

      return (new InvoiceLine())
        ->setId($adjustment->getSourceId())
        ->setItem($productItem)
        ->setPrice($price)
        ->setTaxTotal($taxTotal)
        ->setInvoicedQuantity(1)
        ->setLineExtensionAmount($totalExcl);
    }, $adjustments);
  }

}
