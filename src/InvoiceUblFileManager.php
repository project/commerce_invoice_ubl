<?php

namespace Drupal\commerce_invoice_ubl;

use Drupal\commerce_invoice\Entity\InvoiceInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class InvoiceUblFileManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\commerce_invoice_ubl\InvoiceUblBuilder
   */
  private InvoiceUblBuilder $invoiceUblBuilder;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_invoice_ubl\InvoiceUblBuilder $invoiceUblBuilder
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, InvoiceUblBuilder $invoiceUblBuilder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->invoiceUblBuilder = $invoiceUblBuilder;
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\file\FileInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getInvoiceFile(InvoiceInterface $invoice) {
    // Check if an invoice was already generated for the given invoice,
    // that is not referenced by the invoice.
    $file = $this->loadExistingFile($invoice);
    // If the invoice file hasn't been generated yet, generate it.
    if (!$file) {
      $file = $this->generateInvoiceFile($invoice);
    }

    if (!$file) {
      throw new NotFoundHttpException();
    }

    return $file;
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   */
  protected function generateInvoiceFile(InvoiceInterface $invoice): ?FileInterface {
    try {
      return $this->invoiceUblBuilder->savePrintable($invoice);
    }
    catch (\Exception $e) {
      watchdog_exception('commerce_invoice', $e);
      return NULL;
    }
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   *
   * @return \Drupal\file\FileInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function loadExistingFile(InvoiceInterface $invoice): ?FileInterface {
    /** @var \Drupal\File\FileStorageInterface $file_storage */
    $file_storage = $this->entityTypeManager->getStorage('file');
    // In case the invoice doesn't reference a file, fallback to loading a
    // file matching the given filename.
    $filename = $this->invoiceUblBuilder->generateFilename($invoice);
    $files = $file_storage->loadByProperties([
      'uri' => "private://$filename",
      'langcode' => $invoice->language()->getId(),
    ]);

    if (!$files) {
      return NULL;
    }

    /** @var \Drupal\File\FileInterface $file */
    $file = $file_storage->load(key($files));
    if (!file_exists($file->getFileUri())) {
      $file->delete();
      return NULL;
    }

    return $file;
  }

}
