<?php

namespace Drupal\commerce_invoice_ubl\Controller;

use Drupal\commerce_invoice\Entity\Invoice;
use Drupal\commerce_invoice_ubl\InvoiceUblFileManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class InvoiceController extends ControllerBase {

  /**
   * @var \Drupal\commerce_invoice_ubl\InvoiceUblFileManager
   */
  private InvoiceUblFileManager $invoiceUblFileManager;

  /**
   * @param \Drupal\commerce_invoice_ubl\InvoiceUblFileManager $invoiceUblFileManager
   */
  public function __construct(InvoiceUblFileManager $invoiceUblFileManager) {
    $this->invoiceUblFileManager = $invoiceUblFileManager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\commerce_invoice_ubl\Controller\InvoiceController
   */
  public static function create(ContainerInterface $container): InvoiceController {
    return new static(
      $container->get('commerce_invoice_ubl.invoice_file_manager')
    );
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\Invoice $invoice
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function downloadUbl(Invoice $invoice): BinaryFileResponse {
    $file = $this->invoiceUblFileManager->getInvoiceFile($invoice);

    // Check whether we need to force the download.
    $basename = basename($file->getFileUri());
    $headers = file_get_content_headers($file);
    $headers['Content-Disposition'] = "attachment;filename={$basename}";

    return new BinaryFileResponse($file->getFileUri(), 200, $headers, FALSE);
  }

}
