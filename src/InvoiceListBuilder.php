<?php

namespace Drupal\commerce_invoice_ubl;

use Drupal\commerce_invoice\InvoiceListBuilder as InvoiceListBuilderBase;
use Drupal\Core\Entity\EntityInterface;

class InvoiceListBuilder extends InvoiceListBuilderBase {

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('view')) {
      $options = [
        'language' => $entity->language(),
      ];

      $operations['download_ubl'] = [
        'title' => t('Download UBL'),
        'url' => $entity->toUrl('download_ubl', $options),
      ];
      $operations['download']['title'] = t('Download PDF');
    }

    return $operations;
  }

}
