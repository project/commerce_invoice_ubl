<?php

namespace Drupal\commerce_invoice_ubl;

use Drupal\commerce_invoice\Entity\InvoiceInterface;
use Drupal\commerce_invoice\Event\InvoiceEvents;
use Drupal\commerce_invoice\Event\InvoiceFilenameEvent;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_print\FilenameGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InvoiceUblBuilder {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $fileStorage;

  /**
   * @var \Drupal\commerce_invoice_ubl\UblBuilder
   */
  private UblBuilder $ublBuilder;

  /**
   * @var \Drupal\entity_print\FilenameGeneratorInterface
   */
  private FilenameGeneratorInterface $filenameGenerator;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private EventDispatcherInterface $eventDispatcher;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  private AccountInterface $currentUser;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_invoice_ubl\UblBuilder $ubl_builder
   * @param \Drupal\entity_print\FilenameGeneratorInterface $filename_generator
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, UblBuilder $ubl_builder, FilenameGeneratorInterface $filename_generator, EventDispatcherInterface $event_dispatcher, AccountInterface $current_user) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->ublBuilder = $ubl_builder;
    $this->filenameGenerator = $filename_generator;
    $this->eventDispatcher = $event_dispatcher;
    $this->currentUser = $current_user;
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   *
   * @return string
   */
  public function generateFilename(InvoiceInterface $invoice): string {
    $filename = $this->filenameGenerator->generateFilename([$invoice]);
    $filename .= '-' . $invoice->language()->getId() . '-' . str_replace('_', '', $invoice->getState()->getId());

    // Let the filename be altered.
    $event = new InvoiceFilenameEvent($filename, $invoice);
    $this->eventDispatcher->dispatch($event, InvoiceEvents::INVOICE_FILENAME);

    return $event->getFilename() . '.xml';
  }

  /**
   * @param \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice
   * @param string $scheme
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function savePrintable(InvoiceInterface $invoice, string $scheme = 'private'): ?EntityInterface {
    $filename = $this->generateFilename($invoice);
    $uri = $this->ublBuilder->savePrintable([$invoice], $scheme, $filename);

    if (!$uri) {
      return NULL;
    }

    $file = $this->fileStorage->create([
      'uri' => $uri,
      'uid' => $this->currentUser->id(),
      'langcode' => $invoice->language()->getId(),
    ]);
    $file->setPermanent();
    $file->save();

    return $file;
  }

}
